# go-service-template

Шаблон для локальной разработки в kubernetes. Поднимается postgres и go сервис, 
так же запускается helm hook для миграции данных.

http://127.0.0.1:8080/index - выведет список элементов хранящихся в базе

### Установка зависимостей
- [Minikube](https://kubernetes.io/ru/docs/tasks/tools/install-minikube/)
- [Helm](https://helm.sh/docs/intro/install/)
- [Skaffold](https://skaffold.dev/docs/install/)
- [Delve](https://github.com/go-delve/delve) is a debugger for the Go programming language.
- [k9s](https://github.com/derailed/k9s) provides a terminal UI to interact with your Kubernetes clusters.

Bitnami популярная репа где много готовых сборок, postgres берем оттуда

helm repo add bitnami https://charts.bitnami.com/bitnami

### Minikube
- minikube start --memory 8192 --disk-size='50000mb' --cpus 6 --vm-driver=hyperkit (рекомендую)
- minikube addons enable ingress (включаем ingress) 

### Развертывание в локальном окружении
- skaffold dev --cleanup (пересобирает образ на лету)
- skaffold debug --cleanup --port-forward (запускает debug сервер)
В случае если skaffold не выставил порт для debug выставляем порт наружу сами
```bash
kubectl get po
```

```bash
NAME                              READY   STATUS    RESTARTS   AGE
db-postgresql-0                   1/1     Running   0          62s
service-deploy-6d889f88dc-d5x6t   1/1     Running   1          57s
```
kubectl port-forward pod/service-deploy-6d889f88dc-d5x6t 56268:56268

```bash 
Forwarding from 127.0.0.1:56268 -> 56268
Forwarding from [::1]:56268 -> 56268
```

Инструкция как подключить debug k [goland](https://golangforall.com/ru/post/go-docker-delve-remote-debug.html)



