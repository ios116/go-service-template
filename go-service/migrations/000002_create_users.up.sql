CREATE TABLE IF NOT EXISTS users
(
    id   serial primary key,
    name text not null
);