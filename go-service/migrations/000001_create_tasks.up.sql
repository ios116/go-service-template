CREATE TABLE IF NOT EXISTS tasks
(
    id          serial primary key,
    description text not null
);
insert into tasks (description)
values ('firs item'),
       ('second item');