module go-service

go 1.16

require (
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	github.com/heptiolabs/healthcheck v0.0.0-20180807145615-6ff867650f40
	github.com/jackc/pgx/v4 v4.13.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/prometheus/client_golang v1.11.0
	go.uber.org/zap v1.19.0
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0 // indirect
)
