package wrplg

import (
	"context"

	"go.uber.org/zap"
)

type correlationIDType int

const (
	requestIDKey correlationIDType = iota
	uriKey
)

// WithRqID returns a context which knows its request ID.
func WithRqID(ctx context.Context, requestID string) context.Context {
	return context.WithValue(ctx, requestIDKey, requestID)
}

// WithPath returns a context which knows its path.
func WithPath(ctx context.Context, path string) context.Context {
	return context.WithValue(ctx, uriKey, path)
}

// getZF получает requestID, path данные из контекста.
func getZF(ctx context.Context) []zap.Field {
	fields := make([]zap.Field, 0, 2)
	if val, ok := ctx.Value(requestIDKey).(string); ok {
		fields = append(fields, zap.String("requestID", val))
	}
	if val, ok := ctx.Value(uriKey).(string); ok {
		fields = append(fields, zap.String("uri", val))
	}

	return fields
}

func Logger(ctx context.Context) *zap.Logger {
	return zap.L().With(getZF(ctx)...)
}
