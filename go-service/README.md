# go-service-boilerplate

## Project layout

Принципы и пожелания:

- В usecase используется основная логика работы.

- webserver отвечает за транспорт, он должен оставаться максимально простым.

- webserver и usecase связаны только через интерфейс.

- entities содержит основные структуры данных, только они могут связывать слои напрямую при необходимости.

- Пакеты config, tracing, metrics вспомогательные.

- fixtures пака с фикстурами для тестирования.

Популярный для go [project-layout](https://github.com/golang-standards/project-layout)
```bash
├── deploy (конфигурационные файлы для деплоя)
│   ├── charts
│   └── templates
├── internal (не экспортируемые во вне пакеты)
│   ├── config (конфигурационные файлы с перемнными окружения)
│   ├── entities (слой c основными структурами данных)
│   ├── storage (слой базы данных)
│   ├── usecase (слой внутренней бизнес логики)
│   └── webserver (слой транспорта не связан с внутренней логикой)
└── vendor
    ├── github.com
    ├── go.uber.org
    ├── golang.org
    └── google.golang.org
```
## Параметры

Параметры системы можно задавать через переменные окружения:

- BOILERPLATE_APP_PORT - порт, на котором будет работать приложение, default - 9090
- BOILERPLATE_LOG_MOD - как выводить логи:

      - DebugLevel = -1
      - InfoLevel =  0
      - WarnLevel =  1
      - ErrorLevel = 2
      - DPanicLevel = 3
      - PanicLevel = 4
      - FatalLevel = 5
      
### Переменные для трассировки сервиса:

- BOILERPLATE_TRACE - включение/отключение трассировки OpenTelemetry (Jaeger) - true(1)/false(0), default - false
- BOILERPLATE_JAEGER_HOST - хост сервера Jaeger, пример: 127.0.0.1, default - ""
- BOILERPLATE_JAEGER_PORT - порт сервера Jaeger, пример: 14268, default - ""
- BOILERPLATE_JAEGER_PATH - путь до эндпойнта приема сервера Jaeger, default - /api/traces
- BOILERPLATE_JAEGER_FRACTION_OF_TRACES - значение float64 между 0 - 1 процент запросов для трассировки (например, 0.05 = 5%), default - 1

