// Сервис go-service
//
// Описание REST-api
//
// Host: localhost
// Consumes:
// - application/json
//
// Produces:
// - application/json
//
// swagger:meta
package main

import (
	"go-service/internal/config"
	"go-service/internal/metrics"
	"go-service/internal/storage"
	"go-service/internal/usecase"
	"go-service/internal/webserver"
	"runtime"
	"sync"

	"go.uber.org/zap"
)

// Version версия сервиса устанавливается автоматически на этапе сборки проекта (из переменной окружения VERSION).
var Version string

func main() {
	name := "go-service"
	// настройки для отслеживания блокировок
	runtime.SetBlockProfileRate(1)
	runtime.SetMutexProfileFraction(1)
	// конфиг приложения
	appConfig := config.NewAppConf(Version)
	// создание logger
	logger := appConfig.NewLogger()
	zap.ReplaceGlobals(logger)
	defer func() {
		err := logger.Sync()
		if err != nil {
			zap.L().Warn("logger.Sync() error", zap.Error(err))
		}
	}()
	zap.L().Info("Starting application", zap.String("name", name), zap.String("version", Version))

	// dependency configuration
	// database connection
	dbPool, err := config.DBConnection(appConfig.DBConf)
	if err != nil {
		zap.L().Fatal(err.Error())
	}
	defer func() {
		dbPool.Close()
	}()
	metrics.Start(dbPool)
	// store layer
	repo := storage.NewDBRepo(dbPool, appConfig.DBConf)
	// usecase layer
	logic := usecase.NewProcess(repo)
	// transport layer
	var wg sync.WaitGroup
	wg.Add(1) // for WebServer
	ws := webserver.NewHTTPServer(appConfig, logic)
	go ws.Run(&wg)
	zap.L().Info("Starting application... done")
	wg.Wait()
}
