package storage

import (
	"context"
	"go-service/internal/config"
	"go-service/internal/entities"
	"go-service/wrplg"
	"go.uber.org/zap"

	"github.com/jackc/pgx/v4/pgxpool"
)

type DBRepo struct {
	db   *pgxpool.Pool
	conf config.DBConf
}

func NewDBRepo(db *pgxpool.Pool, conf config.DBConf) *DBRepo {
	return &DBRepo{db: db, conf: conf}
}

// GetAll get all data.
func (d *DBRepo) GetAll(ctx context.Context) ([]entities.Task, error) {
	rows, err := d.db.Query(ctx, "select * from tasks")
	if err != nil {
		wrplg.Logger(ctx).Error("GetAll", zap.Error(err))

		return nil, err
	}
	defer rows.Close()
	tasks := make([]entities.Task, 0)

	for rows.Next() {
		var id int64
		var description string
		if err = rows.Scan(&id, &description); err != nil {
			wrplg.Logger(ctx).Error("GetAll", zap.Error(err))

			return nil, err
		}
		task := entities.Task{
			ID:          id,
			Description: description,
		}
		tasks = append(tasks, task)
	}

	return tasks, rows.Err()
}

// Ping ping connection to database.
func (d *DBRepo) Ping(ctx context.Context) error {
	conn, err := config.DBConnection(d.conf)
	if err != nil {
		return err
	}
	defer conn.Close()

	return conn.Ping(ctx)
}
