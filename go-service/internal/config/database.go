package config

import (
	"context"
	"fmt"
	"net/url"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/log/zapadapter"

	"go.uber.org/zap"

	"github.com/jackc/pgx/v4/pgxpool"
)

type DBConf struct {
	DBPassword  string `envconfig:"POSTGRES_PASSWORD" default:"pass"`
	DBPort      string `envconfig:"POSTGRES_PORT" default:"5432"`
	DBUser      string `envconfig:"POSTGRES_USER" default:"postgres"`
	DBHost      string `envconfig:"POSTGRES_HOST" default:"db-postgresql"`
	DBName      string `envconfig:"POSTGRES_DB" default:"tasks"`
	DBNMaxConns int32  `envconfig:"POSTGRES_MAX_CONNS" default:"10"`
	DBUrl       *url.URL
}

func DBConnection(cfg DBConf) (*pgxpool.Pool, error) {
	var err error
	// urlExample := "postgres://username:password@localhost:5432/database_name"
	cfg.DBUrl, err = url.Parse(fmt.Sprintf("postgres://%s:%s@%s:%s/%s", cfg.DBUser, cfg.DBPassword, cfg.DBHost, cfg.DBPort, cfg.DBName))
	if err != nil {
		return nil, err
	}

	config, err := pgxpool.ParseConfig(cfg.DBUrl.String())
	if err != nil {
		return nil, err
	}
	// config.ConnConfig.RuntimeParams = map[string]string{
	//	"standard_conforming_strings": "on",
	//	"log_statement":               "all",
	// }
	// config.ConnConfig.PreferSimpleProtocol = true
	config.MaxConns = cfg.DBNMaxConns
	config.ConnConfig.Logger = zapadapter.NewLogger(zap.L())
	config.ConnConfig.LogLevel = pgx.LogLevelError
	pool, err := pgxpool.ConnectConfig(context.Background(), config)
	if err != nil {
		return nil, err
	}

	return pool, nil
}
