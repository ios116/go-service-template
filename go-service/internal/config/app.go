package config

import (
	"fmt"
	"log"
	"net/url"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"github.com/kelseyhightower/envconfig"
)

type ConnType string

var CurrentVersion = "unknown"

const (
	envPrefix = "BOILERPLATE"
	levels    = 5
)

var Config *AppConf

// общий конфиг приложения.
type AppConf struct {
	Port string `envconfig:"APP_PORT" default:"9999"`
	// -1 debug режим полный вывод логов
	LogMod  int8   `envconfig:"LOG_MOD" default:"-1"`
	AppName string `envconfig:"APP_NAME" default:"boilerplate"`
	// tracing
	UseTracing             bool    `envconfig:"TRACE" default:"false"`
	JaegerHost             string  `envconfig:"JAEGER_HOST" default:"127.0.0.1"`
	JaegerPort             string  `envconfig:"JAEGER_PORT" default:"14268"`
	JaegerPath             string  `envconfig:"JAEGER_PATH" default:"/api/traces"`
	JaegerFractionOfTraces float64 `envconfig:"JAEGER_FRACTION_OF_TRACES" default:"1"`
	DBConf
	JaegerURL *url.URL
}

func NewAppConf(version string) *AppConf {
	cfg := &AppConf{}
	err := envconfig.Process(envPrefix, cfg)
	if err != nil {
		log.Fatalf("NewAppConf: %v", err)
	}

	if cfg.UseTracing {
		cfg.JaegerURL, err = url.Parse(fmt.Sprintf("http://%s:%s%s", cfg.JaegerHost, cfg.JaegerPort, cfg.JaegerPath))
		if err != nil {
			log.Fatal("invalid JAEGER settings ", err)
		}
	}

	Config = cfg
	if version != "" {
		CurrentVersion = version
	}

	return cfg
}

// NewLogger app logger.
func (app *AppConf) NewLogger() *zap.Logger {
	if app.LogMod > levels {
		log.Fatal("unexpected log level")
	}
	var logger *zap.Logger
	var err error
	if app.LogMod == -1 {
		logger, err = zap.NewDevelopment(zap.IncreaseLevel(zapcore.Level(app.LogMod)))
	} else {
		logger, err = zap.NewProduction(zap.IncreaseLevel(zapcore.Level(app.LogMod)))
	}
	if err != nil {
		log.Fatal(err)
	}

	return logger
}
