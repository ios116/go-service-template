package entities

import "net/http"

// Route находится в entities для разрешения import cycle.
type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}
