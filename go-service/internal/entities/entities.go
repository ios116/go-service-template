package entities

// swagger:model Task
type Task struct {
	ID          int64  `json:"id"`
	Description string `json:"description"`
}

// swagger:model Response
type Response struct {
	Status   string `json:"status"`
	Response string `json:"response"`
}
