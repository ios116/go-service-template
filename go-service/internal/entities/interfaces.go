package entities

import "context"

// IStore слой базы данных.
type IStore interface {
	GetAll(ctx context.Context) ([]Task, error)
	Ping(ctx context.Context) error
}

// IUsecase слой.
type IUsecase interface {
	Ready(ctx context.Context) error
	Live(ctx context.Context) error
	Processing(ctx context.Context) ([]Task, error)
}
