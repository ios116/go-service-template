package usecase

import (
	"context"
	"go-service/internal/entities"
)

type Process struct {
	store entities.IStore
}

func NewProcess(store entities.IStore) *Process {
	return &Process{store: store}
}

// Live probe restart service if connection is fall.
func (p *Process) Live(ctx context.Context) error {
	return nil
}

// Ready probe.
func (p *Process) Ready(ctx context.Context) error {
	return p.store.Ping(ctx)
}

// Processing some logic.
func (p *Process) Processing(ctx context.Context) ([]entities.Task, error) {
	return p.store.GetAll(ctx)
}
