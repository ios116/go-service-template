package webserver

import (
	"context"
	"encoding/json"
	"go-service/internal/config"
	"net/http"

	"github.com/heptiolabs/healthcheck"
)

func (s *HTTPServer) Readiness() http.HandlerFunc {
	ctx := context.Background()
	health := healthcheck.NewHandler()
	health.AddReadinessCheck("service:", func() error {
		return s.Usecase.Ready(ctx)
	})

	return health.ReadyEndpoint
}

func (s *HTTPServer) Liveness() http.HandlerFunc {
	ctx := context.Background()
	health := healthcheck.NewHandler()
	health.AddLivenessCheck("service:", func() error {
		return s.Usecase.Live(ctx)
	})

	return health.LiveEndpoint
}

// GetAppVersionHandler получение версии сервиса.
func (s *HTTPServer) GetAppVersionHandler(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)

	versionNum := make(map[string]string, 1)
	versionNum["version"] = config.CurrentVersion

	encoder := json.NewEncoder(w)
	encoder.SetIndent("", "    ")
	_ = encoder.Encode(versionNum)
}

// MainHandler Получение списка items.
func (s *HTTPServer) MainHandler(w http.ResponseWriter, r *http.Request) {
	// swagger:operation  GET /api/v1/index MainHandler
	// Получение списка items.
	// ---
	// produces:
	// - application/json
	// parameters:
	// - in: query
	//   name: identification
	//   description: 'uuid девайса'
	//   schema:
	//     type: string
	//     format: uuid
	// responses:
	//   '200':
	//     description: все хорошо
	//     schema:
	//       type: array
	//       items:
	//         $ref: '#/definitions/Task'
	//   '400':
	//     description: applications not found
	//     schema:
	//       $ref: '#/definitions/Response'
	//   '500':
	//     description: внутренняя ошибка сервера
	//     schema:
	//       $ref: '#/definitions/Response'
	ctx := context.Background()
	tasks, err := s.Usecase.Processing(ctx)
	if err != nil {
		writeResponse(w, 500, []byte(err.Error()))

		return
	}
	b, _ := json.Marshal(tasks)
	writeResponse(w, 200, b)
}
