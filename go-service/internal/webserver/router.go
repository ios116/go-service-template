package webserver

import (
	"go-service/internal/entities"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"github.com/gorilla/mux"
)

const (
	apiVersion  = "/api/v1/"
	fileDocPath = "./doc/"
	apiDocPath  = apiVersion + "swagger/"
)

func (s *HTTPServer) newRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, route := range s.routing() {
		handler := route.HandlerFunc
		router.Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)
	}
	// отображение документации
	router.PathPrefix(apiDocPath).Handler(http.StripPrefix(apiDocPath, http.FileServer(http.Dir(fileDocPath))))
	// лог всех запросов
	router.Use(s.logMiddleware)

	return router
}

func (s *HTTPServer) routing() []entities.Route {
	return []entities.Route{
		{
			Name:        "Liveness",
			Method:      "GET",
			Pattern:     "/live",
			HandlerFunc: s.Liveness(),
		},
		{
			Name:        "Readiness",
			Method:      "GET",
			Pattern:     "/ready",
			HandlerFunc: s.Readiness(),
		},
		{
			Name:        "Prometheus", // Name
			Method:      "GET",        // HTTP method
			Pattern:     "/metrics",   // Route pattern
			HandlerFunc: promhttp.Handler().(http.HandlerFunc),
		},
		{
			Name:        "GetAppVersion",
			Method:      "GET",
			Pattern:     "/version",
			HandlerFunc: s.GetAppVersionHandler,
		},
		{
			Name:        "MainHandler",
			Method:      "GET",
			Pattern:     "/index",
			HandlerFunc: s.metricsMiddleware(s.MainHandler),
		},
	}
}
