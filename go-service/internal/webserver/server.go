package webserver

import (
	"fmt"
	"go-service/internal/config"
	"go-service/internal/entities"
	"net/http"
	"sync"

	"go.uber.org/zap"
)

type HTTPServer struct {
	AppConfig *config.AppConf
	Usecase   entities.IUsecase
}

func NewHTTPServer(httpConfig *config.AppConf, usecase entities.IUsecase) *HTTPServer {
	return &HTTPServer{AppConfig: httpConfig, Usecase: usecase}
}

func (s *HTTPServer) Run(wg *sync.WaitGroup) {
	defer wg.Done()
	dsn := fmt.Sprintf(":%s", s.AppConfig.Port)
	r := s.newRouter()
	http.Handle("/", r)
	server := &http.Server{Addr: dsn, Handler: nil}
	zap.L().Info("Server is running:", zap.String("port", s.AppConfig.Port))
	if err := server.ListenAndServe(); err != nil {
		zap.L().Info("Server failed:", zap.String("err", err.Error()))
	}
}
