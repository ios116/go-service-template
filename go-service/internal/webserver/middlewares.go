package webserver

import (
	"fmt"
	"go-service/internal/metrics"
	"go-service/wrplg"
	"net/http"
	"strconv"
	"time"

	"go.uber.org/zap"

	"github.com/google/uuid"

	"github.com/prometheus/client_golang/prometheus"
)

func (s *HTTPServer) metricsMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		wrapped := wrapResponseWriter(w)
		next.ServeHTTP(wrapped, r)
		elapsed := float64(time.Since(start).Microseconds())
		metrics.RequestSummary.With(prometheus.Labels{"handler": r.URL.Path, "code": strconv.Itoa(wrapped.status)}).Observe(elapsed / 1000.0)
		metrics.RequestCounter.With(prometheus.Labels{"handler": r.URL.Path, "code": strconv.Itoa(wrapped.status)}).Inc()
	}
}

func (s *HTTPServer) logMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		requestID := uuid.NewString()
		newCtx := wrplg.WithPath(r.Context(), fmt.Sprintf("%s %s", r.Method, r.RequestURI))
		newCtx = wrplg.WithRqID(newCtx, requestID)
		wrapped := wrapResponseWriter(w)
		next.ServeHTTP(wrapped, r.WithContext(newCtx))
		if r.RequestURI != "/live" && r.RequestURI != "/ready" {
			wrplg.Logger(newCtx).Debug("request", zap.Duration("elapsed", time.Since(start)), zap.Int("status", wrapped.status))
		}
	})
}

type responseWriter struct {
	http.ResponseWriter
	status int
}

func wrapResponseWriter(w http.ResponseWriter) *responseWriter {
	return &responseWriter{ResponseWriter: w}
}

func (rw *responseWriter) WriteHeader(code int) {
	rw.status = code
	rw.ResponseWriter.WriteHeader(code)
}
