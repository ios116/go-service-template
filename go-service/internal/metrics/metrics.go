package metrics

import (
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/prometheus/client_golang/prometheus"
	"go.uber.org/zap"
)

const prefix = "go_service"

var (
	RequestSummary = prometheus.NewSummaryVec(prometheus.SummaryOpts{
		Namespace:  prefix,
		Name:       "request_duration_ms",
		Help:       "summery response time for requests to handlers in ms",
		Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.95: 0.005, 0.99: 0.001, 1: 0},
	}, []string{"handler", "code"})
	RequestCounter = prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: prefix,
		Name:      "request_total",
		Help:      "Counter for handlers",
	}, []string{"handler", "code"})
)

func Start(pool *pgxpool.Pool) {
	if err := prometheus.Register(RequestSummary); err != nil {
		zap.L().Error(err.Error())
	}
	if err := prometheus.Register(RequestCounter); err != nil {
		zap.L().Error(err.Error())
	}

	prometheus.MustRegister(prometheus.NewGaugeFunc(
		prometheus.GaugeOpts{
			Namespace: "pg",
			Name:      "acquired_connections",
			Help:      "number of currently acquired connections in the pool.",
		},
		func() float64 { return float64(pool.Stat().AcquiredConns()) },
	))
}
