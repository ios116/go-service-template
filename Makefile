dev:
	skaffold dev --cleanup

debug:
	skaffold debug --cleanup

start:
	minikube start --memory 8192 --disk-size='50000mb' --cpus 6 --vm-driver=hyperkit

delete:
	minikube delete

